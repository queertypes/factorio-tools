-----------------------------------------------------------------------------
-- |
-- Module      :  Main (factorio-tools-spec)
-- Copyright   :  Copyright (C) 2017 Allele Dev
-- License     :  GPL-3 (see the file LICENSE)
-- Maintainer  :  allele.dev@gmail.com
-- Stability   :  provisional
-- Portability :  portable
--
-- This is the primary test suite.
-----------------------------------------------------------------------------
module Main where

import Prelude
import Factorio.Tools

import Hedgehog
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

spec :: IO Bool
spec =
    checkParallel $ Group "Factorio.Tools" [
    ("proper-red-science", withTests 1 $ propRedScienceCraft)
    ]

redScienceCraftSolution :: CraftExpr
redScienceCraftSolution =
  CraftStep (Science Red) (Count 1) Assembler (CraftTime 5) [
    CraftStep (Plate Copper) (Count 2) Furnace (CraftTime 7) [
      CraftStep (Crushed (BaseOre Stiratite)) (Count 4) Crusher (CraftTime 1) [
        CraftStep (Ore Stiratite) (Count 4) Miner (CraftTime 1) []]],
    CraftStep (Component IronGearWheel) (Count 1) Assembler (CraftTime 1) [
      CraftStep (Plate Iron) (Count 2) Furnace (CraftTime 7) [
        CraftStep (Crushed (BaseOre Saphirite)) (Count 4) Crusher (CraftTime 1) [
          CraftStep (Ore Saphirite) (Count 4) Miner (CraftTime 1) []]]]]

propRedScienceCraft :: Property
propRedScienceCraft =
  property $ do
    let solution = solve (Science Red) (Count 1) Main.recipes
    solution === redScienceCraftSolution

recipes :: [Recipe]
recipes = [
      Recipe (CraftTime 7)
             (Inputs [(Crushed (BaseOre Stiratite), Count 3)])
             (Outputs [(Plate Copper, Count 2)])
             Furnace

    , Recipe (CraftTime 7)
             (Inputs [(Crushed (BaseOre Saphirite), Count 3)])
             (Outputs [(Plate Iron, Count 2)])
             Furnace

    , Recipe (CraftTime 1)
             (Inputs [(Plate Iron, Count 4)])
             (Outputs [(Component IronGearWheel, Count 2)])
             Assembler

    , Recipe (CraftTime 5)
             (Inputs [
                     (Component IronGearWheel, Count 1)
                   , (Plate Copper, Count 1)
                  ])
             (Outputs [(Science Red, Count 1)])
             Assembler

    , Recipe (CraftTime 1)
             (Inputs [])
             (Outputs [(Ore Saphirite, Count 1)])
             Miner

    , Recipe (CraftTime 1)
             (Inputs [])
             (Outputs [(Ore Stiratite, Count 1)])
             Miner

    , Recipe (CraftTime 1)
             (Inputs [(Ore Stiratite, Count 2)])
             (Outputs [
                     (Crushed (BaseOre Stiratite), Count 2)
                   , (Crushed (BaseOre Stone), Count 1)
                   ])
             Crusher

    , Recipe (CraftTime 1)
             (Inputs [(Ore Saphirite, Count 2)])
             (Outputs [
                     (Crushed (BaseOre Saphirite), Count 2)
                   , (Crushed (BaseOre Stone), Count 1)
                   ])
             Crusher

    ]

main :: IO Bool
main = spec
